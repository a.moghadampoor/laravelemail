<?php

namespace App\Rules\Sms;

use App\Rules\Emails\IEmailRule;

class InvoiceSmsRule implements ISmsRule
{

    private $request;

    public function __construct($request)
    {
        $this->request=$request;

    }

    public function validate()
    {
        $this->request->validate([
            'param.invoice_id'=>'required|integer',
            'param.name'=>'required|string',

        ]);
        return true;
    }
}
