<?php

namespace App\Rules\Sms;

class WelcomeSmsRule implements ISmsRule
{

    private $request;

    public function __construct($request)
    {
        $this->request = $request;

    }

    public function validate(): bool
    {
        $this->request->validate([
            'param.name' => 'required|string',

        ]);
        return true;
    }
}
