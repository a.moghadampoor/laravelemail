<?php

namespace App\Rules\Sms;

class LoginfailedSmsRule implements ISmsRule
{

    private $request;

    public function __construct($request)
    {
        $this->request=$request;

    }

    public function validate(): bool
    {
        $this->request->validate([
            'param.tries'=>'required|integer',
            'param.name'=>'required|string',
        ]);
        return true;
    }
}
