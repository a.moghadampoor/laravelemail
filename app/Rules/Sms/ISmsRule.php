<?php

namespace App\Rules\Sms;

interface ISmsRule
{
    public function __construct($request);
    public function validate();
}
