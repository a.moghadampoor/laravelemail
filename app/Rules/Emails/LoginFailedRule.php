<?php

namespace App\Rules\Emails;

class LoginFailedRule implements IEmailRule
{

    private $request;

    public function __construct($request)
    {
        $this->request=$request;
    }

    public function valid()
    {
        $this->request->validate([
            'parameters.name'=>'required|string',
            'parameters.tries'=>'required|integer'
        ]);
        return true;
    }
}
