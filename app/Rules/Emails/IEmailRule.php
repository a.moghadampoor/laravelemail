<?php

namespace App\Rules\Emails;


use Illuminate\Http\Request;

interface IEmailRule
{
    public function __construct($request);

    public function valid();
}
