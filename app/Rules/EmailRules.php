<?php

namespace App\Rules;

use App\Models\Template;
use App\Rules\Emails\InvoiceRule;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class EmailRules implements Rule
{
    /**
     * @var mixed|null
     */
    protected $request;
    protected $pathview;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request = null)
    {
        $this->request = $request;
        $this->template = $request->template;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $template = $this->template;

        $nameTemplate = Str::studly($template);
        $nameTemplate .= "Mail.blade.php";
        $nameTemplate = resource_path("views/mail/$nameTemplate");
        if (file_exists($nameTemplate)) {
            $className = "App\Rules\Emails\\{$this->request['template']}";
            $className .= "Rule";
            if (class_exists($className)) {
                $class = new $className($this->request);
                $class->valid();
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;


//
//        $nameTemplate=Str::studly($this->template);
//
//        $nameTemplate.="Mail.blade.php";
//
//        $nameTemplate=resource_path("views/mail/$nameTemplate");
//
//        if (file_exists($nameTemplate)){
//            $className="App\Rules\Emails\\{$this->request['template']}";
//            $className.="Rule";
//            if (class_exists($className)){
//               $class= new $className($this->request);
//                $class->valid();
//            }else{
//                return false;
//            }
//        }else{
//            return false;
//        }
//        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Template Not Exist!';
    }
}
