<?php

namespace App\Console\Commands;

use App\Mail\LoginMail;
use App\Repositores\Database\EmailRepositore;
use App\Repositores\Database\FailedRepositore;
use App\Repositories\EmailRepository;
use App\Services\EmailServiec;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

/**
 * @property  $email
 */
class LoginMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:login';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command For sent Email Login';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $emailRepositore;
    /**
     * @var EmailServiec
     */
    protected $emailService;

    public function __construct()
    {
        parent::__construct();
        $this->emailRepositore = new EmailRepositore();
        $this->emailService = new EmailServiec();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $login = $this->emailRepositore->getStatusPendingOrwere('type', 'login');
        $this->emailService->sentEmail($login,LoginMail::class);
        return 0;
    }


}
