<?php

namespace App\Console\Commands;

use App\Mail\TemplateMail;
use App\Repositores\Database\EmailRepositore;
use App\Repositores\Database\FailedRepositore;
use App\Services\EmailServiec;
use Illuminate\Console\Command;

class WelcomeMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:welcome';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command For sent Email Welcome';


    /**
     * @var EmailRepositore
     */
    protected $emailRepositore;
    /**
     * @var EmailServiec
     */
    protected $emailService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->emailRepositore = new EmailRepositore();
        $this->emailService = new EmailServiec();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $login = $this->emailRepositore->getStatusPendingOrwere('type', 'welcome');
        $this->emailService->sentEmail($login,TemplateMail::class);
        return 0;
    }

}
