<?php

namespace App\Console\Commands;

use App\Mail\InvoiceMail;
use App\Repositores\Database\EmailRepositore;
use App\Repositores\Database\FailedRepositore;
use App\Services\EmailServiec;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;


class InvoiceMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:invoice';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'This Command For Send Pending Emails';

    protected $emailRepositore;
    /**
     * @var EmailServiec
     */
    protected $emailService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->emailRepositore = new EmailRepositore();
        $this->emailService = new EmailServiec();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $login = $this->emailRepositore->getStatusPendingOrwere('type', 'invoice');
        $this->emailService->sentEmail($login,InvoiceMail::class);
    }

}

