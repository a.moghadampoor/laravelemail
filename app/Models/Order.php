<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    const STATUS_PENDING='pending';
    const STATUS_PAID='paid';
    const STATUS_CANCELED='canceld';
    protected $fillable=[
        'invoice_id',
        'user_id',
        'product',
        'status',
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
