<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliverSms extends Model
{
    use HasFactory;
    protected $table='deliver_test';
    protected $guarded=[];
}
