<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    const STATUS_PENDING='pending';
    const STATUS_PAID='paid';
    const STATUS_CANCELED='canceld';

    protected $fillable=[
        'amount',
        'status',
        'paid_at',
        'expired_at',
    ];

    public function order()
    {
        return $this->hasOne(Order::class);
    }
}
