<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $table = 'services';
    const STATUS_PENDING=1;
    const STATUS_CANCELED=2;
    const STATUS_ACTIVE=3;
    const STATUS_EXPIRED=4;

    const REMIND_HALF_DAY=12;
    const REMIND_DAY=24;
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }


}
