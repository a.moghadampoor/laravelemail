<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServiceResource;
use App\Jobs\ServiceJob;
use App\Models\Service;
use App\Repositores\Database\ServiceRepositore;
use App\Repositores\Database\UserRepositore;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceController extends Controller
{
    /**
     * @var Service
     */
    protected $modelrepositore;

    public function __construct()
    {
        $this->modelrepositore = new ServiceRepositore();
    }

    public function index()
    {
        return ServiceResource::collection($this->modelrepositore->with());
    }

    public function getStatusActive()
    {
        return ServiceResource::collection($this->modelrepositore->get());

    }

    public function checkAmount()
    {

        //return($this->modelrepositore->where());
//        return collect($this->modelrepositore->where())->map(function ($item) {
//            var_dump($item->user->cradit . '>=' . $item->product->cycle->amount);
//            var_dump($item->id);
//
//            if ($item->expiret_at < now() && $item->user->cradit >= $item->product->cycle->amount) {
//
//                try {
//                    DB::beginTransaction();
//                    $item->expiret_at = Carbon::now()->addHour(1);
//                    $item->user->cradit -= $item->product->cycle->amount;
//                    $item->user->save();
//                    $item->save();
//                    DB::commit();
//                    Log::info('success');
//                } catch (\Exception $e) {
//                    DB::rollBack();
//                    Log::info('no commit');
//                }
//                return $item->expiret_at;
//            } else {
//                $item->status = UserRepositore::STATUS_EXPIRED;
//                $item->save();
//            }
//        });
        ServiceJob::dispatch();
    }


}
