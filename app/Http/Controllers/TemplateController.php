<?php

namespace App\Http\Controllers;

use App\Http\Resources\TemplateResource;
use App\Repositores\Database\EmailTemplateRepository;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * @var EmailTemplateRepository
     */
    protected $model;

    public function __construct()
    {
        $this->model= new EmailTemplateRepository();
    }

    public function index()
    {
      return  TemplateResource::collection($this->model->paginate(1))->response()->header('amirreza', 'True');
    }
}
