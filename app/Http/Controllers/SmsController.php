<?php

namespace App\Http\Controllers;

use App\Models\Sms;
use App\Notifications\SmsNotification;
use App\Rules\SmsRules;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function index(Request $request)
    {

        $data=$request->validate([
           'phonenumber'=> 'required|numeric',
            'type'=>'required|string',
            'param'=>['required','array',new SmsRules($request)]

        ]);
        $send=new Sms();
        $send->notify(new SmsNotification($request->all()));
    }
}
