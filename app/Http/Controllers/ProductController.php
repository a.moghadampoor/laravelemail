<?php

namespace App\Http\Controllers;

use App\Repositores\Database\ProductRepositore;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var ProductRepositore
     */
    protected $modelRepositore;

    public function __construct()
    {
        $this->modelRepositore = new ProductRepositore();
    }

    public function index()
    {

        return  \App\Http\Resources\ProductResource::collection($this->modelRepositore->all());

    }
}
