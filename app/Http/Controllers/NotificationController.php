<?php

namespace App\Http\Controllers;

use App\Jobs\EmailJob;
use App\Models\Template;
use App\Repositores\Database\EmailTemplateRepository;
use App\Rules\EmailRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;

class NotificationController extends Controller
{

    /**
     * @var EmailTemplateRepository
     */
    protected $templaterepository;
    private $template;

    public function __construct()
    {
        $this->templaterepository = new EmailTemplateRepository();
    }

    public function send(Request $request, Template $template)
    {
//        dd($request->template);
        $batchKey = 'SendEmail-' . $request->template;

        $request->validate([
            'email' => 'required|array',
            'template' => ['required', 'string', 'exists:templates,name'],
            'parameters' => ['required', 'array', new EmailRules($request)],
        ]);

        $this->setTemplate($this->templaterepository->getByName($request->template));
        $data = $this->getTemplate();
        if ($data->is_active == false) {
            return response()->json([
                'errors' => [
                    'status' => 403,
                    'title' => 'Template not active',
                    'detail' => 'Template Is Not Active To use'
                ],
            ], '403');
        }

        if (Cache::get($batchKey)) {
            Bus::findBatch(Cache::get($batchKey))->add([
                new EmailJob($request->all(), $data)
            ]);

        } else {
            $batch = Bus::batch(
                new EmailJob($request->all(), $data),
            )->name($batchKey)->dispatch();
            Cache::put($batchKey, $batch->id);
        }


    }

    public function update(Request $request,Template $template)
    {
        $request->validate([
            'is_active' =>'integer'
        ]);
       return $this->templaterepository->update($template,$request->all());
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

}
