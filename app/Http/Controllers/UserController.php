<?php

namespace App\Http\Controllers;

use App\Jobs\EmailJob;
use App\Jobs\RemindJob;
use App\Models\Service;
use App\Models\User;
use App\Repositores\Database\CycleRepositore;
use App\Repositores\Database\ProductRepositore;
use App\Repositores\Database\ServiceRepositore;
use App\Repositores\Database\UserRepositore;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    /**
     * @var User
     */


    protected $templateName;
    protected $modelRepositore;
    /**
     * @var CycleRepositore
     */
    protected $cycleRepositore;
    /**
     * @var ServiceRepositore
     */
    private $serviceRepositore;
    /**
     * @var ProductRepositore
     */
    private $productRepositore;

    public function __construct()
    {
        $this->modelRepositore = new UserRepositore();
        $this->cycleRepositore = new CycleRepositore();
        $this->serviceRepositore = new ServiceRepositore();
        $this->productRepositore = new ProductRepositore();
    }

    public function index()
    {

        return \App\Http\Resources\UserResource::collection($this->modelRepositore->with());
    }

    public function usable($user_id)
    {

        $user = $this->modelRepositore->get($user_id);
        $credit = $user->cradit;
        $servicesActive = $user->services()->whereStatus(Service::STATUS_ACTIVE)->orderBy('expiret_at')->get();

        if (($servicesActive->isEmpty())) {
            return response()->json([
                'errore' => [
                    'service' => 'deactive service',
                    'details' => 'user not have active service',
                ]
            ], 444);
        }
        $cycle = $this->cycleRepositore->get();


        $TotalCycleAmount = 0;
        $servicesActive->each(function ($service) use (&$cycle, &$TotalCycleAmount) {
            $TotalCycleAmount += $cycle[$service->product_id];
        });
        if ($TotalCycleAmount <= $credit) {
            list($remining, $division) = $this->getHour($credit, $TotalCycleAmount);
        }
        $array = [];
        $hour = $division;
        $credit = $remining;
        list($hour, $array) = $this->setHour($servicesActive, $hour, $array);
        $TotalCycleAmount = 0;
        $i = 0;
        while (true) {
            $i++;
            collect($servicesActive)->each(function ($service) use (&$TotalCycleAmount, &$cycle, $credit, &$servicesActive) {
                if ($TotalCycleAmount + $cycle[$service->product_id] <= $credit) {
                    $TotalCycleAmount += $cycle[$service->product_id];
                } else {

                    $servicesActive = collect($servicesActive)->where('id', '<>', $service->id);
                }
            });
            if ($TotalCycleAmount === 0) {
                break;
            }
            list($remining, $division) = $this->getHour($credit, $TotalCycleAmount);
            $hour = $division;
            list($hour, $array) = $this->setHour($servicesActive, $hour, $array);
            $credit = $remining;
            $TotalCycleAmount = 0;
        }

        d($array);
        collect($array)->each(function ($hours, $productId) use ($array, $user) {
            $product = $this->productRepositore->get($productId);

            if ($hours == Service::REMIND_HALF_DAY) {
//                    d($user->id);
                d("HALF" . $productId);
//                RemindJob::dispatch($user, $productId, $hours, 'Remindhalf');
                $batchKey = 'SendEmail-' . 'Remindhalf';
                if (Cache::get($batchKey)) {
                    Bus::findBatch(Cache::get($batchKey))->add([
                        new RemindJob($user, $productId, $hours, $product, 'Remindhalf', 'Reminder'),
                    ]);
                } else {
                    //    dd(Cache::get($batchKey));
                    $batch = Bus::batch(
                        new RemindJob($user, $productId, $hours, $product, 'Remindhalf', 'Reminder'),
                    )->name($batchKey)->dispatch();
                    Cache::put($batchKey, $batch->id);
                }

            }
            if ($hours == Service::REMIND_DAY) {
                d('DAY');
            }
        });

        return $array;
    }

    public function remind()
    {

        $data = [];
        $user = $this->modelRepositore->all();

        $user->each(function ($model, $key) use (&$data, &$count) {

            // d($model->id);
            $this->usable($model->id);

        });


    }

    /**
     * @param $credit
     * @param $TotalCycleAmount
     * @return int[]
     */
    public function getHour($credit, $TotalCycleAmount): array
    {
        $division = (int)($credit / $TotalCycleAmount);
        $remining = ($credit % $TotalCycleAmount);
        return array($remining, $division);
    }

    /**
     * @param $services
     * @param int $hour
     * @param array $array
     * @return array
     */
    public function setHour($services, int $hour, array $array): array
    {
        collect($services)->each(function ($service) use (&$hour, &$array) {
            $array[$service->product_id] = $hour;
        });
        return array($hour, $array);
    }


}


















