<?php

namespace App\Http\Controllers;

use App\Http\Resources\CycleResource;
use App\Repositores\Database\CycleRepositore;
use Illuminate\Http\Request;

class CycleController extends Controller
{
    /**
     * @var CycleRepositore
     */
    protected $modelRepositore;

    public function __construct()
    {
        $this->modelRepositore=new CycleRepositore();
    }

    public function index()
    {
        return \App\Http\Resources\CycleResource::collection($this->modelRepositore->all());
    }
}
