<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'user'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'cradit'=>$this->cradit,
            'service'=> \App\Http\Resources\ServiceResource::collection($this->whenLoaded('service')),
        ];
    }

}
