<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'service'=>$this->id,
            'userId'=> new UserResource($this->whenLoaded('user')),
            'productId'=>new  ProductResource($this->product),
            'amount'=>$this->product->cycle->amount,
            'statusService'=>$this->status,
            'start'=>$this->actived_at,
            'expiret'=>$this->expiret_at,
            'created'=>$this->created_at,
            'updated'=>$this->updated_at,
        ];
    }
}
