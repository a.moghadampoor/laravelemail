<?php

namespace App\Http\Resources;

use App\Models\Template;
use Illuminate\Http\Resources\Json\JsonResource;

class TemplateResource extends JsonResource
{


    public $preserveKeys = true;
    public $collects =Template::class;
    public static $wrap = 'template';

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'=>$this->name,
            'view'=>$this->view_map,
            'title'=>$this->subject,
            'status'=>$this->is_active,
            'required'=>$this->regex(),
        ];

    }

    public function regex()
    {
        $re = '/\{\$(?<parameters>[A-Za-z0-9_]+)\}/m';
        preg_match_all($re, $this->subject, $matches);
       return $matches['parameters'];
    }

}
