<?php

namespace App\Repositores\Database;

use App\Models\Service;
use App\Models\User;

class UserRepositore
{

    /**
     * @var User
     */
    protected $model;

    const STATUS_PENDING = 1;
    const STATUS_CANCELED = 2;
    const STATUS_ACTIVE = 3;
    const STATUS_EXPIRED = 4;


    public function __construct()
    {
        $this->model = new User();
    }

    public function with()
    {
        return $this->model->whereHas('services', function ($service) {
            return $service->services->orderBy('expiret_at', 'asc');
        })->get();
    }


    public function get($user_id)
    {
        return ($this->model
            ->where('id', $user_id)
            ->first()
        );
    }
    public function all()
    {
        return ($this->model
            ->with('services')
            ->get()
        );
    }

    public function getService()
    {
        return ($this->model->services())->whereStatus(Service::STATUS_ACTIVE)->orderBy('expiret_at')->get();
    }

    public function getUSerServiceActive()
    {
        return $this->model->service()
            ->where('status', Service::STATUS_ACTIVE)
            ->orderBy('expiret_at', 'asc')
            ->get();
    }

}




