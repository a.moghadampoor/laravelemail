<?php

namespace App\Repositores\Database;

use App\Models\Product;

class ProductRepositore
{
    /**
     * @var Product
     */
    protected $model;

    public function __construct()
    {
        $this->model = new Product();
    }

    public function with()
    {
      return  $this->model->with('service')->get();
    }

    public function all()
    {
        return $this->model->all();
    }
  public function get($id)
    {
        return $this->model->where('id','=',$id)->first();
    }


}
