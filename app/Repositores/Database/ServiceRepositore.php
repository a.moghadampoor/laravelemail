<?php

namespace App\Repositores\Database;

use App\Models\Service;
use App\Models\User;
use Carbon\Carbon;

class ServiceRepositore
{

    /**
     * @var Service
     */
    protected $model;

    public function __construct()
    {
        $this->model = new Service();
    }

    public function get()
    {
        return $this->model->with('user')->whereStatus(UserRepositore::STATUS_ACTIVE)->where('expiret_at', '<', now())->get();
    }

    public function with()
    {
        return $this->model->with('user')->get();
    }

    public function update($id)
    {
        return $this->model->where('user_id', '=', $id)->update([
            'expiret_at' => Carbon::now()->addHours(1),
        ]);
    }

    public function where()
    {
        return $this->model
            ->with(['user', 'product'])
            ->where('status', '=', UserRepositore::STATUS_ACTIVE)
            ->orderBy('expiret_at','asc')
            ->get();
    }

    public function getActiveServiceUser($user_id)
    {
        return $this->model
            ->where('user_id' , $user_id)
            ->where('status' , Service::STATUS_ACTIVE)
            ->orderBy('expiret_at' , 'asc')
            ->get();
    }




}
