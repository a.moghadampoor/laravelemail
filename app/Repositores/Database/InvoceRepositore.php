<?php

namespace App\Repositores\Database;

use App\Models\Invoice;
use App\Models\Order;

class InvoceRepositore
{
    /**
     * @var Invoice
     */
    protected $invoiceRepositore;

    public function __construct()
    {
        $this->invoiceRepositore= new Invoice();
    }
    public function create($data)
    {
        return $this->invoiceRepositore->create($data);
    }

    public function update(Invoice $model,$data)
    {
        return tap($model)->update($data);
    }


}
