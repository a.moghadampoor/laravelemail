<?php

namespace App\Repositores\Database;

use App\Models\Invoice;
use App\Models\Order;

class OrderRepositore
{

    protected $orderRepositore;

    public function __construct()
    {
        $this->orderRepositore = new Order();
    }

    public function create($data)
    {
        return $this->orderRepositore->create($data);
    }

    public function update(Order $model,$data)
    {
        return tap($model)->update($data);
    }

}
