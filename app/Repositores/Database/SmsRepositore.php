<?php

namespace App\Repositores\Database;

use App\Models\Sms;

class SmsRepositore
{
    /**
     * @var Sms
     */
    protected $model;

    public function __construct()
    {
        $this->model=new Sms();
    }

    public function get($type)
    {
        return $this->model->whereType($type)->first();
    }
}
