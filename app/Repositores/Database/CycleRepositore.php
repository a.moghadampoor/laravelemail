<?php

namespace App\Repositores\Database;

use App\Models\Cycle;

class CycleRepositore
{
    /**
     * @var Cycle
     */
    protected $model;

    public function __construct()
    {
        $this->model = new Cycle();
    }

    public function all()
    {
        return $this->model->all();
    }


    public function get(): \Illuminate\Support\Collection
    {
        return $this->model->all()->pluck('amount','product_id');
    }

}
