<?php

namespace App\Repositores\Database;

use App\Models\Template;
use phpDocumentor\Reflection\Types\This;

class EmailTemplateRepository
{
    /**
     * @var Template
     */
    private $model;


    public function __construct()
    {
        $this->model = new Template();
    }

    public function getByName($template)
    {
        return $this->model->whereName($template)->first();
    }

    public function index()
    {
        return $this->model->all();
    }

    public function paginate()
    {
        return Template::paginate();
    }

    public function get($template)
    {
        return $this->model->whereId($template->id)->first();
    }

    public function update(Template $template,$data)
    {

         return tap($template)->update($data);
    }
}
