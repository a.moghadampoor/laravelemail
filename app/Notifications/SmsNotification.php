<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SmsNotification extends Notification
{
    use Queueable;

    protected $data;
    public $phoneNumber;
    public $type;

    /**
     * @var mixed
     */
    protected $param;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($data)
    {

       $this->setPhoneNumber($data['phonenumber']);
       $this->setType($data['type']);
       $this->param= $data['param'];
    }

    public function via(): array
    {
        return [SmsChannel::class];
    }

    public function toSms(): array
    {

        return [
            'phone_number'=>$this->getPhoneNumber(),
            'type'=>$this->getType(),
            'param'=>$this->param,
        ];
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber): SmsNotification
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getType():string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string  $type)
    {
        $this->type = $type;

    }


}
