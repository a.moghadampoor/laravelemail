<?php

namespace App\Services;

use App\Mail\InvoiceMail;
use App\Repositores\Database\EmailRepositore;
use App\Repositores\Database\FailedRepositore;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer\PHPMailer;

class EmailServiec
{
    protected $phpmailer;
    /**
     * @var EmailRepositore
     */
    protected $email;
    /**
     * @var FailedRepositore
     */
    protected $fail;

    public function __construct()
    {
        $this->email = new EmailRepositore();
        $this->fail = new FailedRepositore();
    }

    /**
     * @param $Data
     * @return void
     */
    public function sentEmail($Data,$email): void
    {
        $Data->each(function ($item) use($email) {
            echo $item->id . ' - ' . $item->email . PHP_EOL;
            try {
                Mail::to($item)->send(new $email($item, $item->id));
                $this->email->updateEmailDone($item->id);
            } catch (\Exception $e) {
                $count = $this->fail->count($item->id);
                $this->fail->ifCount($count, $item, $item->type, $this->email);
                d($this->fail->count($item->id));
            }
        });
    }

}
