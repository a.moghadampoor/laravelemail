<?php

namespace App\Services;

use Ghasedak\GhasedakApi;

class GhasedakService
{

    public function ghasedak($phone_number,  $body)
    {

        try {
            $api = new GhasedakApi(env('GHASEDAKAPI_KEY'));
            $api->SendSimple(
                $phone_number,
                $body,
                "300002525"
            );
        } catch (\Exception $e) {
            d($e->getMessage());
        }
    }
}
