<?php

namespace App\Providers;

use App\Models\Invoice;
use App\Models\Service;
use App\Models\Template;
use App\Observers\InvoiceObserver;
use App\Observers\ServiceObserver;
use App\Observers\TemplateObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Template::observe(TemplateObserver::class);
        Invoice::observe(InvoiceObserver::class);
        Service::observe(ServiceObserver::class);

    }
}
