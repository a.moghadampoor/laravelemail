<?php

namespace App\Jobs;

use App\Mail\TemplateMail;
use App\Mail\TemplateRemindMail;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class RemindJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Batchable;

    private $templatename;
    private $productId;
    private $houres;
    private $email;
    private $product;
    private $suject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users, $hours, $productId, $product, $templatename,$subject)
    {
        $this->product=$product;
        $this->email = $users->email;
        $this->templatename = $templatename;
        $this->productId = $productId;
        $this->houres = $hours;
        $this->suject=$subject;

        //    $this->cycle=
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $batchKey = 'SendEmail-' . 'Remindhalf';
        if (Bus::findBatch(Cache::get($batchKey))->canceled()) {
            $this->fail();
        } else {
            Mail::to($this->email)->send(new TemplateRemindMail($this->productId, $this->houres,$this->product, $this->templatename ,$this->suject));
        }
    }

}
