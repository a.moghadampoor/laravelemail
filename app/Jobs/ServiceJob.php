<?php

namespace App\Jobs;

use App\Repositores\Database\ServiceRepositore;
use App\Repositores\Database\UserRepositore;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $modelrepositore;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->modelrepositore = new ServiceRepositore();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        collect($this->modelrepositore->where())->each(function ($item) {

            if ($item->expiret_at < now() && $item->user->cradit >= $item->product->cycle->amount) {
                try {
                    DB::beginTransaction();
                    $item->expiret_at = Carbon::now()->addHour(1);
                    $item->user->cradit -= $item->product->cycle->amount;
                    $item->user->save();
                    $item->save();
                    DB::commit();
                    Log::info('success');
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::info('no commit');
                }
            } else {
                $item->status = UserRepositore::STATUS_EXPIRED;
                $item->save();
            }
        });
    }
}
