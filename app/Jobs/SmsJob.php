<?php

namespace App\Jobs;

use Ghasedak\GhasedakApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $phoneNumber;
    private $body;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($phoneNumbe, $body)
    {
        $this->phoneNumber = $phoneNumbe;
        $this->body = $body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $api = new GhasedakApi(env('GHASEDAKAPI_KEY'));
            $api->SendSimple(
                $this->phoneNumber,
                $this->body,
                "300002525"
            );
        } catch (\Exception $e) {
            d($e->getMessage());
        }
    }
}
