<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TemplateRemindMail extends Mailable
{
    use Queueable, SerializesModels;

    private $productId;
    private $houre;
    private $templatename;
    public $product;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($productId,$houre,$product,$templatename ,$subject)
    {
        $this->subject=$subject;
        $this->product=$product;
        $this->productId=$productId;
        $this->houre=$houre;
        $this->templatename=$templatename."Mail";

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.'.$this->templatename);
    }
}
