<?php

namespace App\Broadcasting;

use App\Jobs\SmsJob;
use App\Models\DeliverSms;
use App\Models\Sms;
use App\Models\User;
use App\Notifications\SmsNotification;
use App\Repositores\Database\SmsRepositore;
use App\Services\GhasedakService;
use Ghasedak\GhasedakApi;
use Illuminate\Support\Str;

class SmsChannel
{
    /**
     * @var SmsRepositore
     */
    protected $smsRepository;

    public function __construct()
    {
        $this->smsRepository = new SmsRepositore();
        $this->ghasedakService = new GhasedakService();

    }

    public function send($notifiable, SmsNotification $notification)
    {
        $data = $this->getArgumentNotification($notification, $notifiable);
        $sms = $this->smsRepository->get($data['type']);
        $body = $this->createBody($notifiable, $sms->body, $notification);
//        $this->ghasedakService->ghasedak($data['phone_number'], $body);
        SmsJob::dispatch($data['phone_number'], $body);
    }

    public function createBody($notifiable, $body, SmsNotification $notification): string
    {
        $str = $body;
        $data = $this->getArgumentNotification($notification, $notifiable);
        collect($data['param'])->each(function ($value, $key) use (&$str) {
            $str = Str::replace('{$' . $key . '}', "$value", $str);
        });

        return $str;
    }

    /**
     * @param SmsNotification $notification
     * @param $notifiable
     * @return array
     */
    public function getArgumentNotification(SmsNotification $notification, $notifiable): array
    {
        $data = $notification->toSms($notifiable);
        return $data;
    }

    /**
     * @param $phone_number
     * @param string $body
     * @return void
     */

}
