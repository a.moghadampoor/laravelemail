<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/email', [\App\Http\Controllers\Api\EmailController::class, 'index']);
Route::post('/email/bulk', [\App\Http\Controllers\Api\EmailController::class, 'bulk']);
Route::post('/pending', [\App\Http\Controllers\Api\EmailController::class, 'show']);
Route::post('/email/send', [\App\Http\Controllers\NotificationController::class, 'send']);

Route::post('/invoice/create', [\App\Http\Controllers\InvoiceController::class, 'create']);
Route::post('/order/create', [\App\Http\Controllers\OrderController::class, 'create']);

Route::put('/email/{template}', [\App\Http\Controllers\NotificationController::class, 'update']);
Route::put('/invoice/{invoice}', [\App\Http\Controllers\InvoiceController::class, 'update']);

Route::get('email/get', [\App\Http\Controllers\TemplateController::class, 'index']);
Route::get('sms', [\App\Http\Controllers\SmsController::class, 'index']);
Route::get('/users/{template}', function (\App\Models\Template $template) {
    return new \App\Http\Resources\TemplateResource((new \App\Repositores\Database\EmailTemplateRepository())->get($template));
});
Route::get('/test', function (\App\Models\User $model) {

    dd($model->with('service')->get());
});
//Route::get('/user', function (\App\Models\User $model) {
//
//
//    return \App\Http\Resources\UserResource::collection($model->with('service')->get());
//});
//Route::get('/user',[\App\Http\Controllers\UserController::class,'show']);
//Route::get('/product', function (\App\Models\Product $model) {
//
//
//    return \App\Http\Resources\ProductResource::collection($model->all());
//});
//Route::get('/service', function (\App\Models\Service $model) {
//      $service= new \App\Models\Service();
//      dd ($service->index());
//    return \App\Http\Resources\ServiceResource::collection($model->with('user')->get());
//});

//Route::get('/cycle', function (\App\Models\Cycle $model) {
//
//
//    return \App\Http\Resources\CycleResource::collection($model->all());
//});


Route::prefix('/user')
    ->controller(\App\Http\Controllers\UserController::class)
    ->group(function () {
        Route::get('/', 'index');
        Route::get('/{user_id}/usable', 'usable');
        Route::get('/remind', 'remind');
        Route::post('/store', 'store');
        Route::put('/update/{invoice}', 'update');
    });

Route::prefix('/service')
    ->controller(\App\Http\Controllers\ServiceController::class)
    ->group(function () {
        Route::get('/', 'index');
        Route::get('/test', 'checkAmount');
        Route::get('/active', 'getStatusActive');
        Route::get('/expired', 'getExpired');
        Route::post('/store', 'store');
        Route::put('/update/{invoice}', 'update');
    });

Route::prefix('/cycle')
    ->controller(\App\Http\Controllers\CycleController::class)
    ->group(function () {
        Route::get('/', 'index');
        Route::post('/store', 'store');
        Route::put('/update/{invoice}', 'update');
    });

Route::prefix('/product')
    ->controller(\App\Http\Controllers\ProductController::class)
    ->group(function () {
        Route::get('/', 'index');
        Route::post('/store', 'store');
        Route::put('/update/{invoice}', 'update');
    });




