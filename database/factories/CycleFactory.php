<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CycleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => 1,
            'amount' => $this->faker->randomElement([10000, 20000, 30000]),
            'period' => 'hourly',
        ];
    }
}
