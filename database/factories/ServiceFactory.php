<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'product_id' => 1,
            'status' => $this->faker->randomElement([1, 2, 3]),
            'actived_at'=>Carbon::now()->subDay()->addHours(rand(1,10))->addMinutes(rand(1,60))->addSeconds(rand(1,60)),
            'expiret_at'=>Carbon::now()->addHours(rand(1,10))->addMinutes(rand(1,60))->addSeconds(rand(1,60)),
        ];
    }
}
