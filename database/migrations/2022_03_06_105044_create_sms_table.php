<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->id();
            $table->text('body');
            $table->boolean('is_active')->nullable();
            $table->timestamps();
        });
        Schema::create('deliver_test', function (Blueprint $table) {
            $table->id();
            $table->string('type_sms',100);
            $table->string('phone_number',15);
            $table->boolean('status')->nullable();
           $table->dateTime('received_at')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
        Schema::dropIfExists('deliver_test');
    }
}
