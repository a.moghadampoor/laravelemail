<?php

namespace Database\Seeders;

use App\Models\Cycle;
use App\Models\Product;
use App\Models\Service;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(10)
            ->create()->each(function ($item) {
                Service::factory()
                    ->create(['user_id' => $item->id]);
            });
    }
}
