<?php

namespace Database\Seeders;

use App\Models\Email;
use Database\Factories\CycleFactory;
use Database\Factories\ProductFactory;
use Database\Factories\ServiceFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Event;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            CycleSeeder::class,
            ProductSeeder::class,
            ServiceSeeder::class,
            UserSeeder::class,
        ]);
    }
}
